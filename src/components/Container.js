import React from 'react'
import FunctionDisplayKey from './FunctionDisplayKey';
import { useState } from 'react';


function Container() {
const [show, setShow] = useState(true);
   const btnDisplay =  show ? 'Cacher' : 'Afficher'

   const clickHandler = () => {
       setShow (! show);
   }
    return (
        <div >
            <button className="btn btn-primary m-3" onClick={clickHandler} >{btnDisplay}</button>
           {
               show &&  <FunctionDisplayKey />
           }
        </div>
    )
}

export default Container
