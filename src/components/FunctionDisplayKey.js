import React from 'react'
import { useState , useEffect } from 'react';
function FunctionDisplayKey() {

const [keyCode, setkeyCode] = useState('');
 
const handlekeyCode = (e) => {
    setkeyCode(e.code);
    console.log('active')

}

useEffect(()=> {
    document.addEventListener("keyup", handlekeyCode)

    return () => {
        document.removeEventListener("keyCode",handlekeyCode);
        console.log('retiré')
    }
}, [])

    return (
        <div className="card m-5">
            <div className="card-body">
                <h1 className="text-center">
                    <h2 >Click on the keyboard </h2> <br /> {keyCode}
                </h1>

            </div>
             
        </div>
    )
}

export default FunctionDisplayKey
